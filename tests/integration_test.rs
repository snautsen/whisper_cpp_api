use axum::{body::Bytes, extract::State, http::StatusCode};
use whisper_cpp_api::{config::Config, route_handler::api::whisper};

#[tokio::test]
async fn test_whisper_handles_spawn_error() {
    let config = Config {
        whisper_path: String::from("/nonexistent/bin"),
        port: 1234,
        whisper_model_path: String::from("foo"),
        whisper_model: String::from("bar"),
        whisper_lang: String::from("baz"),
        max_body_limit: 1234,
    };

    assert_eq!(
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            String::from("failed to spawn child: No such file or directory (os error 2)")
        ),
        whisper(State(config), Bytes::new()).await
    );
}
