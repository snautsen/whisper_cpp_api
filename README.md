# whisper.cpp api

Wraps `main` of [whisper.cpp](https://github.com/ggerganov/whisper.cpp) into HTTP-server. Posting wav as binary to `/api/whisper` returns detected text as response.

## Configuration

See [.env.example](.env.example)

## License

[MIT](https://choosealicense.com/licenses/mit/)