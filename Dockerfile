FROM rust:1.77-slim-bullseye as builder

WORKDIR /usr/src/myapp

COPY . .

RUN cargo install --path .

FROM ghcr.io/ggerganov/whisper.cpp:main

# RUN apt-get update && apt-get install -y extra-runtime-dependencies && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/cargo/bin/whisper_cpp_api /app/whisper_cpp_api

EXPOSE 3000/tcp

ENTRYPOINT [ "/app/whisper_cpp_api" ]