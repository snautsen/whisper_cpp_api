use crate::config::Config;
use axum::body::Bytes;
use axum::extract::State;
use axum::http::StatusCode;
use std::process::Child;
use std::{
    self,
    io::Write,
    process::{Command, Stdio},
    str::{self},
};

pub async fn whisper(State(config): State<Config>, body: Bytes) -> (StatusCode, String) {
    let model = format!(
        "{}/{}.bin",
        &config.whisper_model_path, &config.whisper_model
    );

    let parameters = vec!["-nt", "-m", &model, "-l", &config.whisper_lang, "-"];

    let child: Result<Child, std::io::Error> = Command::new(&config.whisper_path)
        .args(&parameters)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn();

    let mut child = match child {
        Ok(child) => child,
        Err(error) => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("failed to spawn child: {}", error.to_string()),
            )
        }
    };

    let stdin = child.stdin.take();

    if let None = stdin {
        return (
            StatusCode::INTERNAL_SERVER_ERROR,
            String::from("failed to open stdin."),
        );
    }

    let stdin_write_result = stdin.unwrap().write_all(&body);

    if let Err(error) = stdin_write_result {
        return (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("failed writing to stdin: {}", error.to_string()),
        );
    }

    let output = child.wait_with_output();

    match output {
        Ok(output) => match str::from_utf8(&output.stdout) {
            Ok(val) => (StatusCode::OK, val.trim().to_string()),
            Err(_) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                String::from("got non UTF-8 data from whisper"),
            ),
        },
        Err(_) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            String::from("failed to read stdout"),
        ),
    }
}
