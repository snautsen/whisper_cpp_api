use envconfig::Envconfig;

#[derive(Clone, Envconfig)]
pub struct Config {
    #[envconfig(from = "WHISPER_API_PORT", default = "3000")]
    pub port: u16,

    #[envconfig(from = "WHISPER_MODEL_PATH", default = "/models")]
    pub whisper_model_path: String,

    #[envconfig(from = "WHISPER_MODEL", default = "ggml-medium")]
    pub whisper_model: String,

    #[envconfig(from = "WHISPER_BIN", default = "/app/main")]
    pub whisper_path: String,

    #[envconfig(from = "WHISPER_LANG", default = "en")]
    pub whisper_lang: String,

    #[envconfig(from = "MAX_BODY_LIMIT", default = "1048570")]
    pub max_body_limit: usize,
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn test_config_can_be_loaded_from_hashmap() {
        // Create a HashMap that looks like your environment
        let mut hashmap = HashMap::new();
        hashmap.insert("WHISPER_API_PORT".to_string(), "65535".to_string());
        hashmap.insert("WHISPER_MODEL_PATH".to_string(), "/foo/bar".to_string());
        hashmap.insert("WHISPER_MODEL".to_string(), "foo-baz".to_string());
        hashmap.insert("WHISPER_BIN".to_string(), "bin".to_string());
        hashmap.insert("WHISPER_LANG".to_string(), "fr".to_string());
        hashmap.insert("MAX_BODY_LIMIT".to_string(), "1412412414".to_string());

        // Initialize config from a HashMap to avoid test race conditions
        let config = Config::init_from_hashmap(&hashmap).unwrap();

        assert_eq!(config.port, 65535);
        assert_eq!(config.whisper_model_path, "/foo/bar");
        assert_eq!(config.whisper_model, "foo-baz");
        assert_eq!(config.whisper_lang, "fr");
        assert_eq!(config.max_body_limit, 1412412414);
    }
}
