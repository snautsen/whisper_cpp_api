use axum::{extract::DefaultBodyLimit, routing::post, Router};
use envconfig::Envconfig;
//use tower_http::limit::RequestBodyLimitLayer;
use whisper_cpp_api::{config::Config, route_handler::api::whisper};

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let config = Config::init_from_env().unwrap();

    let listener = tokio::net::TcpListener::bind(format!("0.0.0.0:{}", config.port))
        .await
        .unwrap();

    let app = Router::new()
        .route("/api/whisper", post(whisper))
        .layer(DefaultBodyLimit::max(10 * 1000 * 1000))
        .with_state(config);

    tracing::debug!("listening on {}", listener.local_addr().unwrap());

    axum::serve(listener, app).await.unwrap();
}
